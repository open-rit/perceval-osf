# -*- coding: utf-8 -*-
#
# Copyright (C) 2015-2020 Bitergia
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program. If not, see <http://www.gnu.org/licenses/>.
#
# Authors:
#     
#

import logging
from typing import Any, Dict, Generator, Optional, Tuple
from perceval.archive import Archive

from grimoirelab_toolkit.datetime import str_to_datetime
from grimoirelab_toolkit.uris import urijoin
from datetime import datetime
from ...backend import (Backend,
                        BackendCommand,
                        BackendCommandArgumentParser,
                        DEFAULT_SEARCH_FIELD)
from ...client import HttpClient, RateLimitHandler
from ...utils import DEFAULT_DATETIME
from requests.models import Response

# Range before sleeping until rate limit reset
MIN_RATE_LIMIT = 10
# Default sleep time and retries to deal with connection/server problems
DEFAULT_SLEEP_TIME = 1
MAX_RETRIES = 5

CATEGORY_COLLECTION="collection"
CATEGORY_CHANGE = ["collection", "node", "user", "registration", "institution", "license", "log"]
OSF_SEARCH_URL="https://api.osf.io/v2/"


logger = logging.getLogger(__name__)


class Osf(Backend):
    """OSF backend for Perceval.

    This class retrieves the activities from OSF API.

    :param url: the URL of a OSF instance
    :param api_token: OSF API token
    :param tag: label used to mark the data
    :param archive: archive to store/retrieve items
    :param max_retries: number of max retries to a data source
        before raising a RetryError exception
    :param sleep_for_rate: sleep until rate limit is reset
    :param min_rate_to_sleep: minimum rate needed to sleep until
         it will be reset
    :param sleep_time: time (in seconds) to sleep in case
        of connection problems
    :param ssl_verify: enable/disable SSL verification
    """
    version = '0.1.0'

    CATEGORIES = CATEGORY_CHANGE

    def __init__(
        self,
        url: str               = OSF_SEARCH_URL,
        api_token: str         = None,
        tag: str               = None,
        archive: Archive       = None,
        sleep_for_rate: bool   = False,
        min_rate_to_sleep: int = MIN_RATE_LIMIT,
        max_retries: int       = MAX_RETRIES,
        sleep_time: int        = DEFAULT_SLEEP_TIME,
        ssl_verify: bool       = True,
        node_id: str           = None
    ) -> None:
        super().__init__(url, tag=tag, archive=archive, ssl_verify=ssl_verify)
        
        self.api_token = api_token
        self.max_retries = max_retries
        self.sleep_time = sleep_time
        self.sleep_for_rate = sleep_for_rate
        self.min_rate_to_sleep = min_rate_to_sleep
        self.client: Optional[OsfClient] = None
        self.node_id = node_id

    def search_fields(self, item: Dict[str, Any]) -> Dict[str, str]:
        """Add search fields to an item.

        It adds the values of `metadata_id` plus the `project`.

        :param item: the item to extract the search fields values

        :returns: a dict of search fields
        """
        search_fields = {
            DEFAULT_SEARCH_FIELD: self.metadata_id(item)
        }

        return search_fields

    def fetch(
        self,
        category: str = CATEGORY_COLLECTION,
        from_date: datetime = DEFAULT_DATETIME
    ) -> Generator[Dict[str, Any], None, None]:
        """Fetch changes from OSF API.

        The method retrieves the activity that occurred on a project via the OSF API.

        :param category: the category of items to fetch

        :returns: a generator of data
        """
        
        if category in ['log']:
            assert self.node_id is not None, "A node_id must be provided to retrieve node-specific categories"
            self.url = self.origin + 'nodes/' + self.node_id + '/' + category + 's'
        else:
            assert self.node_id is None, "A node_id mustn't be provided for global categories"
            self.url = self.origin + category + 's'

        if not from_date:
            from_date = DEFAULT_DATETIME

        kwargs = {
            'from_date': from_date
        }
        
        items = super().fetch(category, **kwargs)
        
        return items

    def fetch_items(self, category: str, **kwargs: datetime) -> Generator[Dict[str, Any], None, None]:
        """Fetch change items

        :param category: the category of items to fetch
        :param kwargs: backend arguments

        :returns: a generator of items
        """
        from_date: datetime = kwargs['from_date']
        logger.info("Fetching %s items on '%s' from '%s", category, self.url, from_date)
        logger.info("Fetch process completed")
        
        if self.client is None:
            raise RuntimeError("Cannot call Osf.fetch_items() before defining Osf.client")

        payload = {}
        if from_date is not None:
            payload[self.client.PAFTER] = from_date.isoformat()

        return self.client.fetch_items(self.client.base_url, payload)

    @classmethod
    def has_archiving(cls) -> bool:
        """Returns whether it supports archiving items on the fetch process.

        :returns: this backend supports items archive
        """
        return True

    @classmethod
    def has_resuming(cls) -> bool:
        """Returns whether it supports to resume the fetch process.

        :returns: this backend supports items resuming
        """
        return True
    
    @staticmethod
    def metadata_id(item: Dict[str, Any]) -> str:
        """Extracts the identifier from an item."""
        #print(item)

        return str(item['id'])

    @staticmethod
    def metadata_updated_on(item: Dict[str, Any]) -> float:
        """Extracts the update time from an item.

        The timestamp used is extracted from 'timestamp' field.
        This date is converted to UNIX timestamp format taking into
        account the timezone of the date.

        :param item: item generated by the backend

        :returns: a UNIX timestamp
        """
        try:
            ts = item['attributes']['date_modified']
            ts = str_to_datetime(ts)
        except:
            ts=datetime.now(tz=None)

        
        

        return ts.timestamp()

    @staticmethod
    def metadata_category(item: Dict[str, Any]) -> str:
        """Extracts the category from a OSF item.

        One of "collection", "node", "user", "registration",
        "institution", or "license"
        """
        return item['type'][:-1]

    def _init_client(self, from_archive: bool = False) -> 'OsfClient':
        """Init client"""

        return OsfClient(self.url, self.api_token,
                             self.sleep_for_rate, self.min_rate_to_sleep, self.sleep_time, self.max_retries,
                             archive=self.archive, from_archive=from_archive, ssl_verify=True)


class OsfClient(HttpClient, RateLimitHandler):
    """OsfClient API client.

    Client for fetching activities data from OSF API.

    :param origin: URL of a OSF instance
    :param api_token: OSF API token
    :param sleep_for_rate: sleep until rate limit is reset
    :param min_rate_to_sleep: minimun rate needed to sleep until
         it will be reset
    :param sleep_time: time (in seconds) to sleep in case
        of connection problems
    :param max_retries: number of max retries to a data source
        before raising a RetryError exception
    :param archive: an archive to store/read fetched data
    :param from_archive: it tells whether to write/read the archive
    :param ssl_verify: enable/disable SSL verification
    """
    # Resource parameters
    PAFTER = 'timestamp_after'

    def __init__(
        self,
        origin: str,
        api_token: Optional[str],
        sleep_for_rate: bool = False,
        min_rate_to_sleep: int = MIN_RATE_LIMIT,
        sleep_time: int = DEFAULT_SLEEP_TIME,
        max_retries: int = MAX_RETRIES,
        archive: Optional[Archive] = None,
        from_archive: bool = False,
        ssl_verify: bool = True
    ) -> None:
        if '?' in origin:
            url=origin+'&format=json'
        else:
            url = urijoin(origin, '?format=json')
        self.api_token = api_token

        super().__init__(url, sleep_time=sleep_time, max_retries=max_retries,
                         extra_headers=self._set_extra_headers(),
                         archive=archive, from_archive=from_archive, ssl_verify=ssl_verify)
        super().setup_rate_limit_handler(sleep_for_rate=sleep_for_rate, min_rate_to_sleep=min_rate_to_sleep)

    def _set_extra_headers(self) -> Dict[str, str]:
        """Set extra headers for session"""

        headers = {}
        if self.api_token:
            headers = {'Authorization': "Bearer %s" % self.api_token}

        return headers

    def fetch_items(self, path: str, payload: Optional[Dict[str, str]]) -> Generator[Dict[str, Any], None, None]:
        """Return the items from OSF API using links pagination"""

        current_page = 1  # current page
        fetch = True
        url_next = path
        logger.debug("Get OSF paginated items from " + url_next)
        response = self.fetch(url_next, payload=payload)

        while fetch:
            page = response.json()
            results = page.get('data', [])
            #print(results)
            try:
                page_length = len(page['links'])
            except:
                page_length=1
            logger.debug("Count: %i/%i, page %s" % (len(results), page_length, current_page))
            if page_length>1:
                for r in results:
                    yield r
            else:
                yield results
            try:
                if not page['links']['next']:
                    fetch = False
                    continue
                
                current_page += 1
                url_next = page['links']['next']
                response = self.fetch(url_next)
            except:
                fetch=False

    def calculate_time_to_reset(self) -> float:
        """Number of seconds to wait. They are contained in the rate limit reset header"""

        time_to_reset = 0 if self.rate_limit_reset_ts / 1000 < 0 else self.rate_limit_reset_ts / 1000
        return time_to_reset

    def fetch(
        self,
        url: str,
        payload: Optional[Dict[str, str]] = None,
        headers: Dict[str, str] = None,
        method: str = HttpClient.GET,
        stream: bool = False,
        auth: Optional[Tuple[str, str]] = None
    ) -> Response:
        """Fetch the data from a given URL.

        :param url: link to the resource
        :param payload: payload of the request
        :param headers: headers of the request
        :param method: type of request call (GET or POST)
        :param stream: defer downloading the response body until the response content is available
        :param auth: auth of the request

        :returns a response object
        """
        
        if not self.from_archive:
            self.sleep_for_rate_limit()
        
        print(url, payload, headers, method, stream, auth)

        response = super().fetch(url, payload, headers, method, stream, auth)
        

        if not self.from_archive:
            self.update_rate_limit(response)

        return response


class OsfCommand(BackendCommand):
    """Class to run OSF backend from the command line."""

    BACKEND = Osf

    @classmethod
    def setup_cmd_parser(cls) -> BackendCommandArgumentParser:
        """Returns the OSF argument parser."""

        parser = BackendCommandArgumentParser(cls.BACKEND,
                                              archive=True,
                                              from_date=True,
                                              ssl_verify=True,
                                              token_auth=True,
                                              )

        # OSF options
        group = parser.parser.add_argument_group('Osf arguments')
        group.add_argument('--url',
                            default=OSF_SEARCH_URL,
                            help="OSF URL")
        
        group.add_argument('--sleep-for-rate', dest='sleep_for_rate',
                           action='store_true',
                           help="sleep for getting more rate")
        group.add_argument('--min-rate-to-sleep', dest='min_rate_to_sleep',
                           default=MIN_RATE_LIMIT, type=int,
                           help="sleep until reset when the rate limit reaches this value")

        # Generic client options
        group.add_argument('--max-retries', dest='max_retries',
                           default=MAX_RETRIES, type=int,
                           help="number of API call retries")
        group.add_argument('--sleep-time', dest='sleep_time',
                           default=DEFAULT_SLEEP_TIME, type=int,
                           help="sleeping time between API call retries")

        group.add_argument('--sleep-times', dest='sleep_time',
                           default=DEFAULT_SLEEP_TIME, type=int,
                           help="sleeping time between API call retries")

        # Positional arguments
        group.add_argument('node_id', type=str, nargs='?',
                           help="The node ID of the project to be queried.  If not provided, global" +
                           " statistics will be gathered.")

        return parser
