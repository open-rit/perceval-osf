# perceval-osf 

Bundle of Perceval backends for [**OSF**](https://osf.io)

[**OSF API Docs**](https://developer.osf.io)

## Backends

The backends currently managed by this package support the next repositories:

* OSF

## Requirements

* Python >= 3.6
* python3-requests >= 2.7
* grimoirelab-toolkit >= 0.1.12
* perceval >= 0.17.1

## Installation

To install this package you will need to clone the repository first:

```
$ git clone https://gitlab.com/open-rit/perceval-osf.git
```

Then you can execute the following commands:
```
$ pip3 install -r requirements.txt
$ pip3 install -e .
```

In case you are a developer, you should execute the following commands to install Perceval in your working directory (option `-e`) and the packages of requirements_tests.txt.
```
$ pip3 install -r requirements.txt
$ pip3 install -r requirements_test.txt
$ pip3 install -e .
```

## Examples

### OSF

```
$ perceval osf --category CATEGORY
```
#### Valid OSF categories
- "collections"
- "nodes"
- "users"
- "registrations"
- "institutions"
- "licenses"

#### Category Fetch Examples
```
$  perceval osf --category 'collections'
$  perceval osf --category 'nodes'
```
#### User Auth
```
$ perceval osf -t <APItoken>
```
#### Filtering
```
$ perceval osf --url "https://api.osf.io/v2/<category>/?filter[<field_name>]=<matching_information>"
```
#### Filtering Examples
```
$ perceval osf --url "https://api.osf.io/v2/collections/?filter[title]=Bookmarks" //requires auth
$ perceval osf --url "https://api.osf.io/v2/users/?filter[full_name]=Mikael"

```
#### Help
```
$ perceval osf -h
```

## License

Licensed under GNU General Public License (GPL), version 3 or later.
